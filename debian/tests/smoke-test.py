#!/usr/bin/env python3
# smoke test

import ijson
import io

events = list(ijson.parse(io.StringIO('{ "key": "value" }')))

print(f'events = {events}')

assert events == [
    ('', 'start_map', None),
    ('', 'map_key', 'key'),
    ('key', 'string', 'value'),
    ('', 'end_map', None)
    ]
